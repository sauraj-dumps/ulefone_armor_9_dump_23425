#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:bd37aeaac244ffc99be2ae07a7f53beb474d061d; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:802641f7a1a694aef6c6dd1a57d782c80678c137 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:bd37aeaac244ffc99be2ae07a7f53beb474d061d && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
