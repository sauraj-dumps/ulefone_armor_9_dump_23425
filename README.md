## Armor_9_EEA-user 10 QP1A.190711.020 root.20210109.121311 release-keys
- Manufacturer: ulefone
- Platform: mt6779
- Codename: Armor_9
- Brand: Ulefone
- Flavor: full_s95v79c2k_gq_p_eea-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: root.20210109.121311
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Armor_9_EEA/Armor_9:10/QP1A.190711.020/root.20210109.121311:user/release-keys
- OTA version: 
- Branch: Armor_9_EEA-user-10-QP1A.190711.020-root.20210109.121311-release-keys
- Repo: ulefone_armor_9_dump_23425


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
